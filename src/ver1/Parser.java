package ver1;

import java.util.ArrayList;

/**
 *
 * @author Alexander Minichino
 *
 *(
     * N = {Program, Stmt, Expr, Term},
     * T = {ID, IF, THEN, ELSE, RELOP, NUMBER, ;, ASSIGN, WHILE, DO},
     * S = Program
     * P = {
         * Prog -> Stmt Prog'
         * Prog'-> ;Stmt Prog' | ε
         * Stmt -> IF Expr THEN Stmt ELSE Stmt
         * Stmt -> ID ASSIGN Expr
         * Stmt -> WHILE Expr DO Stmt
         * Expr -> Term Expr'
         * Expr' -> Relop Term | ε
         * Term -> ID | Number
     * }
 * )
 *
 */
class Parser {
    static int ptr;
    private ArrayList<Token> tokens;

    public Parser(String filePath) {
        Lexer lexicalAnalyzer = new Lexer();
        tokens = new ArrayList<>();

        if (lexicalAnalyzer.initialize(filePath)) {

            Token token;
            try {
                while ((token = lexicalAnalyzer.nextToken()) != null) {
                    if (!token.getName().equalsIgnoreCase("WHITESPACE"))
                        tokens.add(token);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ptr = 0;
            boolean isValid = PROG();
            if (isValid)
                System.out.println("Accepted!");
            else
                System.out.println("Not Accepted!");
        }
    }

    boolean PROG(){
        // Check if 'ptr' to 'ptr+2' is Stmt Prog'
        int fallback = ptr;

        if(STMT() == false) {
            ptr = fallback;
            return false;
        }

        if(PROG_D() == false) {
            ptr = fallback;
            return false;
        }
        return true;
    }
    public boolean PROG_D() {
        // Check if Prog'-> ;Stmt Prog' | ε
        int fallback = ptr;
        if (ptr < tokens.size())
            if(tokens.get(ptr++).getAttribute().equalsIgnoreCase("SEMI")) {
                if(STMT() == false) {
                    ptr = fallback;
                    return false;
                }
                if(PROG_D() == false) {
                    ptr = fallback;
                    return false;
                }
                return true;
            }
        ptr = fallback;
        return true; //Prog'-> ε
     }


    public boolean STMT() {
        // Check if Stmt -> IF Expr THEN Stmt ELSE Stmt | Stmt -> ID ASSIGN Expr | Stmt -> WHILE Expr DO Stmt
        int fallback = ptr;
        switch (tokens.get(ptr++).getName().toUpperCase()){
            case "IF":
                return STMT_IF();
            case "ID":
                return STMT_ID();
            case "WHILE":
                return STMT_WHILE();
            default:
                return false;
        }
    }
    private boolean STMT_IF() {
        // Check if Stmt -> IF Expr THEN Stmt ELSE Stmt
        int fallback = ptr;
        if(EXPR() == false) {
            ptr = fallback;
            return false;
        }
        if(!tokens.get(ptr++).getName().equalsIgnoreCase("THEN")) {
            ptr = fallback;
            return false;
        }
        if(STMT() == false) {
            ptr = fallback;
            return false;
        }
        if(!tokens.get(ptr++).getName().equalsIgnoreCase("ELSE")) {
            ptr = fallback;
            return false;
        }
        if(STMT() == false) {
            ptr = fallback;
            return false;
        }
        return true;
    }



    private boolean STMT_ID() {
        // Check if Stmt -> ID ASSIGN Expr
        int fallback = ptr;
        if(!tokens.get(ptr++).getName().equalsIgnoreCase("ASSIGN")){
            ptr = fallback;
            return false;
        }
        if(EXPR() == false) {
            ptr = fallback;
            return false;
        }
        return true;
    }

    private boolean STMT_WHILE() {
        // Check if Stmt -> WHILE Expr DO Stmt
        int fallback = ptr;
        if(EXPR() == false) {
            ptr = fallback;
            return false;
        }
        if(!tokens.get(ptr++).getName().equalsIgnoreCase("DO")){
            ptr = fallback;
            return false;
        }
        if(STMT() == false) {
            ptr = fallback;
            return false;
        }
        return true;
    }

    private boolean EXPR() {
        //Check if Expr -> Term Expr'
        int fallback = ptr;
        if(TERM() == false) {
            ptr = fallback;
            return false;
        }
        if(EXPR_D() == false) {
            ptr = fallback;
            return false;
        }
        return true;
    }

    private boolean EXPR_D() {
        //Check if Expr' -> Relop Term | ε
        int fallback = ptr;
        if (ptr < tokens.size())
            if(tokens.get(ptr++).getName().equalsIgnoreCase("BIN_OP")) {
                if(TERM() == false) {
                    ptr = fallback;
                    return false;
                }
                return true;
            }
        ptr = fallback;
        return true; // Expr' -> ε
    }

    private boolean TERM() {
        //Check if Term -> ID | Number
        Token term = tokens.get(ptr++);
        if(term.getName().equalsIgnoreCase("ID") || term.getName().equalsIgnoreCase("NUMBER")) {
            return true;
        }
        return false;
    }
}